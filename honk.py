import lib.settings
import discord
import asyncpraw
import asyncio
from os import environ
from lib.searchlib import stringOccurenceSearch
from sys import exit
from datetime import datetime
from lib.logger import constructDiscordLogger
from pathlib import Path
import logging

# Version
version = '2.2'

# Logging
logger = constructDiscordLogger()

def main():
    logger.info('Starting Honk2')

    # <reddit.filereader>
    subs = ''
    logger.info(f"Reading sub list file {Path(environ['REDDIT_SUB_LIST_FILE']).absolute()}")
    with open(environ['REDDIT_SUB_LIST_FILE'], 'r') as file:
        file_content = file.read().split('\n')
        subs = '+'.join([x for x in file_content if x])
    if subs == '':
        print('Empty subs file')
        exit(1)
    keywords = []
    logger.info(f'Reading key word list file {Path(environ["REDDIT_KEYWORD_LIST_FILE"]).absolute()}')
    with open(environ['REDDIT_KEYWORD_LIST_FILE'], 'r') as file:
        for line in file:
            keywords.append(line.strip())
    # </reddit.filereader>

    # <discordConstruct>
    intents = discord.Intents.default()
    intents.message_content = True
    client = discord.Client(intents=intents)
    # </discordConstruct>

    @client.event
    async def on_ready():
        logger.info('Connected to Discord')
        # <reddit> from here down, put in separate async function
        async def reddit():
            try:
                reddit = asyncpraw.Reddit(
                    client_id=environ['REDDIT_CLIENT_ID'],
                    client_secret=environ['REDDIT_CLIENT_SECRET'],
                    user_agent=['REDDIT_USER_AGENT']
                )
                logger.info('Connected to Reddit api')
            except Exception as err:
                logger.warning('Reddit connection failed')
                logger.warning(f"{err}")
            logger.info('Time to honk!~')
            multireddit = await reddit.subreddit(subs)
            while True:
                try:
                    async for submission in multireddit.stream.submissions(skip_existing=True):
                        content = submission.title + ':::' + submission.selftext
                        content = content.lower()
                        if environ['NSFW_FILTER'] == 'True':
                            if submission.over_18:
                                logger.debug('Not considering over_18')
                            else:
                                if stringOccurenceSearch(keywords, content, weight=int(environ['SEARCH_WEIGHT'])):
                                    logger.info(f'Found post: {submission.title}')
                                    author_image = 'https://www.redditstatic.com/avatars/avatar_default_02_FF4500.png'
                                    sub_image = 'https://img.favpng.com/21/14/6/computer-icons-calendar-clip-art-png-favpng-hYgLvxDSTgvMpzbqyBfgEDQG9.jpg'
                                    embed = discord.Embed(title=submission.title,
                                                        url='https://reddit.com/' + submission.permalink,
                                                        description=submission.selftext[0:100] + '...',
                                                        colour=0xff5700)
                                    embed.set_author(name=f"r/{submission.permalink.split('/')[2]}",
                                                url=f"https://reddit.com/r/{submission.permalink.split('/')[2]}",
                                                icon_url = sub_image)

                                    embed.set_footer(text=f"r/{submission.author} @ {datetime.fromtimestamp(submission.created_utc)}",
                                                    icon_url=author_image,
                                                    )

                                    await client.get_channel(int(environ['DISCORD_CHANNEL'])).send(embed=embed)
                                    logger.info(f'Sent message in {environ["DISCORD_CHANNEL"]}')
                                else:
                                    pass
                except:
                    logger.error('Something happened to reddit, starting again')
        await reddit()
        # </reddit>

    @client.event
    async def on_message(message): # Convert to command structure
        if message.content.startswith('!honk'):
            print(f'Honk! from {message.author}')
            await message.reply('Honk!')

    client.run(environ['DISCORD_TOKEN'],log_handler=None)

if __name__ == "__main__":
    main()
