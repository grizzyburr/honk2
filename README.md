# Honk2 (WIP)

*Welcome to Honk2! The loudest boy this side of the lake!*   
*This is currently a work in progress*

# Environment Variables   
*Can be defined in `.env` file*   
| Variable | Purpose|
|---|---|
`DISCORD_TOKEN`|The discord token for your bot
`DISCORD_CHANNEL`|The channel you want messages sent by the bot to go
`REDDIT_CLIENT_ID`|Your reddit api client ID
`REDDIT_CLIENT_SECRET`|Your reddit api client secret
`REDDIT_USER_AGENT`|The user agent for your reddit api connection
`REDDIT_SUB_LIST_FILE`|Path to the text file containing subreddits to monitor
`REDDIT_KEYWORD_LIST_FILE`|Path to the text file containing keywords to search for
`SEARCH_WEIGHT`|The weight applicable to the content searched in reddit via the keywords (*optional, default=1*)

# Command Line Arguments   
Honk is meant to run as a service but there are command line arguments that can be used below   
| Argument | Purpose |
|---|---|
`--generate-env`|Generates a `.env` file in the same folder as `honk.py` that can be used in place of environment variables

# Setup   
Honk is a Discord bot designed to monitor a set of subreddits based on keywords and send a message to a Discord channel. Basic setup process is outlined below

```
> git clone https://gitlab.com/grizzyburr/honk2
> cd honk2
> python3 honk.py --generate-env
> vim .env # Make necessary edits to .env file
> python honk.py
```

> ⚠️ **If you are running this on a server:** It is reccomended to use a process manager such as a systemd service or pm2 