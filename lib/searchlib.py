def stringOccurenceSearch(string_list: list, content: str, weight=1) -> bool:
    content = content.lower()
    matching_list = [x for x in string_list if x in content]
    if len(matching_list) < weight:
        return False
    else:
        return True