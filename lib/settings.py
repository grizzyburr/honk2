from os import environ, path
from sys import exit
from dotenv import load_dotenv
import argparse
from .logger import constructSettingsLogger

# Log
logger = constructSettingsLogger()

expected_envs = [
    'DISCORD_TOKEN',
    'DISCORD_CHANNEL',
    'REDDIT_CLIENT_ID',
    'REDDIT_CLIENT_SECRET',
    'REDDIT_USER_AGENT',
    'REDDIT_SUB_LIST_FILE',
    'REDDIT_KEYWORD_LIST_FILE',
    'SEARCH_WEIGHT',
    'NSFW_FILTER'
]


parser = argparse.ArgumentParser(prog='Honk')
parser.add_argument('--generate-env',help='generate an env file',action='store_true')
args = parser.parse_args()
logger.info('Checking if \'.env\' exists')
if args.generate_env:
    target = '.env'
    if path.exists(target):
        logger.critical(f'.env already exists')
        exit(1)
    else:
        with open(target, 'w') as file:
            for var in expected_envs:
                file.write(f'{var}=\n')
        logger.info(f'{target} created')
        exit(1)


#Load settings from environment
load_dotenv()

configFail = 0
notFoundVars = []
for var in expected_envs:
    if var not in list(environ.keys()):
        notFoundVars.append(var)
        configFail = 1
if configFail == 1:
    logger.critical(f"Vars not found: {', '.join(notFoundVars)}")
