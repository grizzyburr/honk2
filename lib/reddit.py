from lib.logger import constructRedditLogger
import asyncio

logger = constructRedditLogger()

class RedditBot():
    def __init__(self, subfile=None, wordfile=None) -> None:
        if subfile == None or wordfile == None:
            raise TypeError('Invalid type for subfile or wordfile, missing arguments')
        self.subfile = subfile
        self.wordfile = wordfile
        self.subs = self.read_sub_file(self.subfile)
        self.words = self.read_words_file(self.wordfile)

    def read_sub_file(self, file):
        with open(file, 'r') as file:
            content = file.read().split('\n')
            subs = '+'.join([x for x in content if x])
            if subs == '':
                logger.critical('Subs file is empty or invalid')

    def read_words_file(self, file):
        with open(file,'r') as file:
            words = [x.strip().lower() for x in file.read()]
            if words == '':
                logger.critical('Words file empty or invalid')

# import asyncio

# async def reddit():
#     try:
#         reddit = asyncpraw.Reddit(
#             client_id=environ['REDDIT_CLIENT_ID'],
#             client_secret=environ['REDDIT_CLIENT_SECRET'],
#             user_agent=['REDDIT_USER_AGENT']
#         )
#         logger.info('Connected to Reddit api')
#     except Exception as err:
#         logger.warning('Reddit connection failed')
#         logger.warning(f"{err}")
#     logger.info('Time to honk!~')
#     multireddit = await reddit.subreddit(subs)
#     while True:
#         try:
#             async for submission in multireddit.stream.submissions(skip_existing=True):
#                 content = submission.title + ':::' + submission.selftext
#                 content = content.lower()
#                 if environ['NSFW_FILTER'] == 'True':
#                     if submission.over_18:
#                         logger.debug('Not considering over_18')
#                     else:
#                         if stringOccurenceSearch(keywords, content, weight=int(environ['SEARCH_WEIGHT'])):
#                             logger.info(f'Found post: {submission.title}')
#                             author_image = 'https://www.redditstatic.com/avatars/avatar_default_02_FF4500.png'
#                             sub_image = 'https://img.favpng.com/21/14/6/computer-icons-calendar-clip-art-png-favpng-hYgLvxDSTgvMpzbqyBfgEDQG9.jpg'
#                             embed = discord.Embed(title=submission.title,
#                                                 url='https://reddit.com/' + submission.permalink,
#                                                 description=submission.selftext[0:100] + '...',
#                                                 colour=0xff5700)
#                             embed.set_author(name=f"r/{submission.permalink.split('/')[2]}",
#                                         url=f"https://reddit.com/r/{submission.permalink.split('/')[2]}",
#                                         icon_url = sub_image)

#                             embed.set_footer(text=f"r/{submission.author} @ {datetime.fromtimestamp(submission.created_utc)}",
#                                             icon_url=author_image,
#                                             )

#                             await client.get_channel(int(environ['DISCORD_CHANNEL'])).send(embed=embed)
#                             logger.info(f'Sent message in {environ["DISCORD_CHANNEL"]}')
#                         else:
#                             pass
#         except:
#             logger.error('Something happened to reddit, starting again')