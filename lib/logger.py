import logging

class ColoredFormatter(logging.Formatter):
    # Colors
    Red = '\u001b[31;1m'
    darkRed = '\u001b[101;1m'
    Green = '\u001b[32;1m'
    lightBlue = '\u001b[36;1m'
    Blue = '\u001b[34;1m'
    Grey = '\u001b[30;1m'
    White = '\u001b[97;1m'
    Reset = '\u001b[0m'
    Orange = '\u001b[33;1m'
    Purple = '\u001b[35;1m'

    # Formats
    timeFormat = Grey + '[{asctime}]' + Reset + ' '
    nameFormat = Purple + '{name}' + Reset
    messageFormat = White + '{message}' + Reset
    Formats = {
        logging.DEBUG: timeFormat + Grey + '[{levelname:<8}]' + Reset + ' ' + nameFormat + ':' + ' ' + messageFormat,
        logging.INFO: timeFormat + Blue + '[{levelname:<8}]' + Reset + ' ' + nameFormat + ':' + ' ' + messageFormat,
        logging.WARN: timeFormat + Orange + '[{levelname:<8}]' + Reset + ' ' + nameFormat + ':' + ' ' + messageFormat,
        logging.ERROR: timeFormat + Red + '[{levelname:<8}]' + Reset + ' ' + nameFormat + ':' + ' ' + messageFormat,
        logging.CRITICAL: timeFormat + darkRed + '[{levelname:<8}]' + Reset + ' ' + nameFormat + ':' + ' ' + messageFormat
    }

    # Return formatted log
    def format(self,record):
        fmt = self.Formats.get(record.levelno)
        dt_fmt = '%Y-%m-%d %H:%M:%S'
        formatter = logging.Formatter(fmt,dt_fmt,style='{')
        return formatter.format(record)


def constructDiscordLogger(level=logging.INFO):
     logger = logging.getLogger('discord')
     logger.setLevel(level)
     handler = logging.StreamHandler()
     handler.setFormatter(ColoredFormatter())
     logger.addHandler(handler)
     return logger

def constructRedditLogger(level=logging.INFO):
     logger = logging.getLogger('reddit')
     logger.setLevel(level)
     handler = logging.StreamHandler()
     handler.setFormatter(ColoredFormatter())
     logger.addHandler(handler)
     return logger

def constructSettingsLogger(level=logging.INFO):
     logger = logging.getLogger('settings')
     logger.setLevel(level)
     handler = logging.StreamHandler()
     handler.setFormatter(ColoredFormatter())
     logger.addHandler(handler)
     return logger

def constructSearchlibLogger(level=logging.INFO):
     logger = logging.getLogger('searchlib')
     logger.setLevel(level)
     handler = logging.StreamHandler()
     handler.setFormatter(ColoredFormatter())
     logger.addHandler(handler)
     return logger

if __name__ == "__main__":
    logger = constructRedditLogger(level=logging.DEBUG)
    logger.debug('debug')
    logger.info('info')
    logger.warning('warning')
    logger.error('error')
    logger.critical('critical')
